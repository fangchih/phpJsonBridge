<?php

// point to the api endpoint
$api = 'api_url';

// preprocess, depend on the requirement
$action = str_replace("param=", "", $_SERVER['QUERY_STRING']);

// prevent cache
header('Content-Type: application/json; charset=utf-8');
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $content = file_get_contents($api . $action);
    echo $content;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($_POST)
        )
    );
    $context  = stream_context_create($opts);
    $result = file_get_contents($api . $action, false, $context);
    echo $result;
}
